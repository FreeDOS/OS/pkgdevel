#!/bin/bash

# download multiple software download repository CSV files and compare update
# package versions with local copy of git repository projects

# Created 2021-2022 by Jerome Shidel v2.0
# Released to Public Domain

# Script settings
TOOL_VER=1.0

# script auto-configuration stuff
SWD="${PWD}"

[[ "$(uname)" == "Darwin" ]] && MACOS=true || unset MACOS

# Help section

function display_help () {
    echo "usage: ${0##*/} [options]"
    exit 0
}

# generic support functions
# Easier to use external functions like TR, but usually faster to stay in bash

UPPER_CHARS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
LOWER_CHARS='abcdefghijklmnopqrstuvwxyz'
SPACE_CHARS=' '

while [[ ${#SPACE_CHARS} -lt 128 ]] ; do
	SPACE_CHARS="${SPACE_CHARS}${SPACE_CHARS}"
done

function lowerCase () {
    if [[ ${MACOS} ]] ; then
        # SLower method not using ${variable,,}
        local i c o
        for ((i=0;i<${#1};i++)) ; do
            c="${1:${i}:1}"
            if [[ "${c//[${UPPER_CHARS}]}" != "${c}" ]] ; then
                c="${UPPER_CHARS%%${c}*}"
                c="${LOWER_CHARS:${#c}:1}"
            fi
            o="${o}${c}"
        done
        echo "${o}"
    else
        echo "${1,,}"
    fi
}

function upperCase () {
    if [[ ${MACOS} ]] ; then
        # Slower method not using ${variable^^}
        local i c o
        for ((i=0;i<${#1};i++)) ; do
            c="${1:${i}:1}"
            if [[ "${c//[${LOWER_CHARS}]}" != "${c}" ]] ; then
                c="${LOWER_CHARS%%${c}*}"
                c="${UPPER_CHARS:${#c}:1}"
            fi
            o="${o}${c}"
        done
        echo "${o}"
    else
        echo "${1^^}"
    fi
}

function rightPad () {
	if [[ ${3} ]] ; then # TRUE = Also crop at length
		local x="${1}${SPACE_CHARS:0:$(( $2 - ${#1} ))}"
		echo "${x:0:${2}}"
	else
		echo "${1}${SPACE_CHARS:0:$(( $2 - ${#1} ))}"
	fi
}

function leftPad () {
	if [[ ${3} ]] ; then # TRUE = Also crop at length
		local x="${SPACE_CHARS:0:$(( $2 - ${#1} ))}${1}"
		echo "${x:0:${2}}"
	else
		echo "${SPACE_CHARS:0:$(( $2 - ${#1} ))}${1}"
	fi
}

function leftTrim () {
    local x="$@"
    local t=""
    while [[ "${t}" != "${x}" ]] ; do
        t="${x}"
        x="${x#${x%%[![:space:]]*}}"
    done
    echo "${x}"
}

function rightTrim () {
    local x="$*"
    local t=""
    while [[ "${t}" != "${x}" ]] ; do
        t="${x}"
        x="${x%${x##*[![:space:]]}}"
    done
    echo "${x}"
}

function trim () {
    local x=$(rightTrim "${@}")
    x=$(leftTrim "${x}")
    echo "${x}"
}

INDENTING=''
DEBUG=

function setIndent () {
    local denting='    '
    if [[ "${1}" == '+' ]] ; then
        INDENTING="${INDENTING}${denting}"
    else
        INDENTING="${INDENTING:${#denting}}"
    fi
}

function dented () {

    echo "${INDENTING}${@}"

}


function epoch_to_date () {
    if [[ ${MACOS} ]] ; then
        # macOS Version
        date -r ${1} +"%C%y%m%d%H%M.%S"
    else
        # Linux Version
        date -d @${1} +"%C%y%m%d%H%M.%S"
    fi
}

function case_match_file () {
    local i c

    local x="${1%%/*}"
    local u=$(upperCase "${x}")
    local t="${1:$(( ${#x} + 1 ))}"

    for i in "${PWD}"/* ; do
        c=$(upperCase "${i##*/}")
        if [[ "${u}" == "${c}" ]] ; then
            x="${i##*/}"
            break
        fi
    done

    if [[ ${#t} -eq 0 ]] ; then
        echo "${x}"
    elif [[ -d "${x}" ]] ; then
        local hwd="${PWD}"
        cd "${x}"
        echo "${x}/$(case_match_file ${t})"
        cd "${hwd}"
    else
        echo "${x}/${t}"
    fi
}

function get_csv_field () {
    local idx=$1
    local index=0
    local ret=1
    shift
    local line="$@"
    local flag bit field
    while [[ "${line}" != '' ]] ; do
        (( index++ ))
        flag=
        field=
        # parse next field
        while [[ "${line}" != "" ]]  && [[ ! $flag ]]; do
            if [[ "${line:0:1}" == '"' ]] ; then
                line="${line:1}"
                if [[ "${line:0:1}" == '"' ]] ; then
                    field="${field}\""
                    line="${line:1}"
                fi
                bit="${line%%\"*}"
                line="${line:1}"
            else
                bit="${line%%\,*}"
                [[ "${line:${#bit}}" == ',' ]] && flag=plusone
                line="${line:1}"
                [[ ! $flag ]] && flag=done
            fi
            line="${line:${#bit}}"
            field="${field}${bit}"
            # [[ "${line}" == ',' ]] && [[ "${flag}" == "," ]]  && plusone=yes
        done
        if [[ $idx -eq $index ]] ; then
            echo "$field"
            ret=0
            break;
        fi
    done
    return $ret
}

function download () {

    local server="${1#*//}'"
    local x="${1%/*}.csv"
    server="${server%%/*}"
    local out="${server#*.}"
    x="${x##*/}"
    out="${out%.*}-${x}"
    [[ -e "${out}" ]] && return 0
    dented "download listing.csv from ${1%%//*}//${server} as ${out}"
    local cmd="$(command -v wget)"
    if [[ -e "${cmd}" ]] ; then
        "${cmd}" -q "${1}" -O "${out}"
        return $?
    fi
    cmd="$(command -v curl)"
    if [[ -e "${cmd}" ]] ; then
        "${cmd}" -s -o "${out}" "${1}"
        return $?
    fi
    echo "Unable to locate either wget or curl, cannot download file."
    return 1
}

function fetch_csv_files () {

	echo Retrieving latest translation CSV files.
	local i=0
	while [[ "${REPO_URL[${i}]}" != '' ]] ; do
            setIndent +
	    download "${REPO_URL[${i}]}"
	    setIndent -
	    if [[ $? -ne 0 ]] ; then
		echo "problem downloading ${url_main}"
		return 1
	    fi
	    (( i++ ))
	done
    return 0
}

function get_csv_field () {
    local idx=$1
    local index=0
    local ret=1
    shift
    local line="$@"
    local flag bit field
    while [[ "${line}" != '' ]] ; do
        (( index++ ))
        flag=
        field=
        # parse next field
        while [[ "${line}" != "" ]]  && [[ ! $flag ]]; do
            if [[ "${line:0:1}" == '"' ]] ; then
                line="${line:1}"
                if [[ "${line:0:1}" == '"' ]] ; then
                    field="${field}\""
                    line="${line:1}"
                fi
                bit="${line%%\"*}"
                line="${line:1}"
            else
                bit="${line%%\,*}"
                [[ "${line:${#bit}}" == ',' ]] && flag=plusone
                line="${line:1}"
                [[ ! $flag ]] && flag=done
            fi
            line="${line:${#bit}}"
            field="${field}${bit}"
            # [[ "${line}" == ',' ]] && [[ "${flag}" == "," ]]  && plusone=yes
        done
        if [[ $idx -eq $index ]] ; then
            echo "$field"
            ret=0
            break;
        fi
    done
    return $ret
}

function package_list () {
    unset PKGID PKGVERSION PKGTITLE
    PKGCNT=0
    local i=0  line x fid ftitle fversion
    [[ ${VERBOSE} ]] && echo "Processing cvs package listing file."
    while IFS=''; read -r line ; do
        if [[ ${i} -eq 0 ]] ; then
            while [[ ${#line} -gt 0 ]] ; do
                (( i++))
                x="$(lowerCase ${line%%,*})"
                line="${line:$((${#x} + 1))}"
                # echo ${x}
                case "${x}" in
                    'id')
                        fid=${i}
                        ;;
                    'title')
                        ftitle=${i}
                        ;;
                    'version')
                        fversion=${i}
                        ;;
                esac
            done
            if [[ "${fid}" == "" ]] ||  [[ "${ftitle}" == "" ]] || [[ "${fversion}" == "" ]] ; then
                echo "error processing cvs header">&2
                exit 1
            fi
            continue
        fi
        PKGID[${PKGCNT}]=$(get_csv_field ${fid} "${line}")
        PKGTITLE[${PKGCNT}]=$(get_csv_field ${ftitle} "${line}")
        PKGVERSION[${PKGCNT}]=$(get_csv_field ${fversion} "${line}")
        # echo "${PKGID[${PKGCNT}]}, ${PKGTITLE[${PKGCNT}]}, ${PKGVERSION[${PKGCNT}]}"
        (( PKGCNT++ ))
    done < "${1}"
    return 0
}

function lsm_field () {

    local lsm="${1##*/}"
    local field line linex a b ignore

    while IFS=''; read -r line ; do
        line="${line//</&lt;}"
        line="${line//>/&gt;}"
        line="$(trim ${line})"
        linex="$(lowerCase ${line})"
        [[ "${linex}" == "end" ]] && ignore=yes
        if [[ "${ignore}" == 'no' ]] ; then
            a=$"${linex%%:*}"
            b="$(trim ${line:$(( ${#a} + 1 ))})"
            a="$(trim ${a})"
            [[ "${b}" == "" ]] && continue
            [[ "${b}" == "-" ]] && continue
            [[ "${b}" == "?" ]] && continue
            [[ "${a}" != "$2" ]] && continue
            field="$b"
            break
        fi
        [[ "${linex}" == "begin3" ]] && ignore=no
    done< "$1"
    [[ "${field}" == "" ]] && echo "${3}" || echo "${field}"
}

function process () {
	package_list "${1}"
	echo "${1} contains ${PKGCNT} packages" | tee -a report.txt
	local i=0 x lsm ver name a b
	unset MISSING VERS VERSNUM
	MISSING_CNT=0
	VERS_CNT=0
	setIndent +
	dented 'version comparison' | tee -a report.txt
	setIndent +
	while [[ ${i} -lt ${PKGCNT} ]] ; do
		if [[ ! -d "${PKGID[${i}]}/.git" ]] ; then
			MISSING[${MISSING_CNT}]="${i}"
			(( MISSING_CNT++ ))
			(( i++ ))
			continue
		fi
		lsm=$(case_match_file "${PKGID[${i}]}/APPINFO/${PKGID[${i}]}.LSM")
		if [[ ! -f "${lsm}" ]] ; then
			echo missing lsm metadata file ${lsm}
			return 1
		fi
		ver=$(lsm_field "${lsm}" "version")
		if [[ "${PKGVERSION[${i}]}" == "${ver}" ]] ; then
			name="${PKGTITLE[${i}]}"
			a=$(upperCase "${PKGID[${i}]}")
			b=$(upperCase "${PKGTITLE[${i}]}")
			[[ "${a}" != "${b}" ]] && name="(${PKGID[${i}]}) ${name}"
			dented "${name}, same, ${ver}" | tee -a report.txt
		else
			VERS[${VERS_CNT}]="${i}"
			VERSNUM[${VERS_CNT}]="${ver}"
			(( VERS_CNT++ ))
		fi
		(( i++  ))
	done
	setIndent -
	if [[ ${VERS_CNT} -eq 0 ]] ; then
		dented 'no differences' | tee -a report.txt
	else
		if [[ ${VERS_CNT} -eq 1 ]] ; then
			dented "one is different" | tee -a report.txt
		else
			dented "${VERS_CNT} are different" | tee -a report.txt
		fi
		setIndent +
			i=0
		while [[ ${i} -lt ${VERS_CNT} ]] ; do
			x=${VERS[${i}]}
			name="${PKGTITLE[${x}]}"
			a=$(upperCase "${PKGID[${x}]}")
			b=$(upperCase "${PKGTITLE[${x}]}")
			[[ "${a}" != "${b}" ]] && name="(${PKGID[${x}]}) ${name}"
			dented "${name}, (csv) ${PKGVERSION[${x}]}, (local) ${VERSNUM[${i}]}" | tee -a report.txt
			(( i++  ))
		done
		setIndent -
	fi

	if [[ ${MISSING_CNT} -ne 0 ]] ; then
		dented "${MISSING_CNT} exist only in download repository (no local version)" | tee -a report.txt
		i=0
		setIndent +
		while [[ ${i} -lt ${MISSING_CNT} ]] ; do
			x=${MISSING[${i}]}
			name="${PKGTITLE[${x}]}"
			a=$(upperCase "${PKGID[${x}]}")
			b=$(upperCase "${PKGTITLE[${x}]}")
			[[ "${a}" != "${b}" ]] && name="(${PKGID[${x}]}) ${name}"
			dented "${name}, ${PKGVERSION[${x}]}" | tee -a report.txt
			(( i++  ))
		done
		setIndent -
	fi

	unset MISSING
	MISSING_CNT=0
	dented "checking repo csv for missing packages"
	i=0
	local n=0
	for x in * ; do
		[[ ! -d "${x}/.git" ]] && continue
		name="${x##/*}"
		a=$(upperCase "${name}")
		while [[ ${PKGCNT} -gt 0 ]] ; do
			b=$(upperCase "${PKGID[${i}]}")
			if [[ "${a}" == "${b}" ]] ; then
				n=${i}
				name=
				break
			fi
			(( i++ ))
			[[ ${i} -eq ${PKGCNT} ]] && i=0
			[[ ${i} -eq ${n} ]] && break
		done
		[[ "${name}" == '' ]] && continue
		MISSING[${MISSING_CNT}]="${name}"
		(( MISSING_CNT++ ))
	done

	if [[ ${MISSING_CNT} -ne 0 ]] ; then
		dented "${MISSING_CNT} exist only locally (not in repo csv)" | tee -a report.txt
		i=0
		setIndent +
		while [[ ${i} -lt ${MISSING_CNT} ]] ; do
			x=${MISSING[${i}]}
			dented "${x}" | tee -a report.txt
			(( i++  ))
		done
		setIndent -
	fi
	setIndent -
	return 0
}

REPO_CNT=0

function include_csv () {
	REPO_URL[${REPO_CNT}]="${*}"
	(( REPO_CNT++ ))
}

function main () {
	[[ -e report.txt ]] && rm report.txt
	fetch_csv_files || return $?
	for csv in *.csv ; do
		[[ ! -f "${csv}" ]] && continue
		process "${csv}" || return $?
	done
	return 0
}

# comment out, remove or add any additional repository CSV URLs to check
# (not checked in order of inclusion)
include_csv "https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/repositories/unstable/listing.csv"
include_csv "https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/repositories/1.3/listing.csv"
include_csv "https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/repositories/1.2/listing.csv"
include_csv "http://fd.lod.bz/repos/current/listing.csv"

main "${@}"
exit $?
