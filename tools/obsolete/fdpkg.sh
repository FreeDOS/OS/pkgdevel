#!/bin/sh

LFN_ZIP=true

if [[ "$(uname)" == "Darwin" ]] ; then
	DARWIN=yes
else
	unset DARWIN
fi

UPPER_CHARS='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
LOWER_CHARS='abcdefghijklmnopqrstuvwxyz'
function upperCase () {

	local s="${*}"
	if [[ ${DARWIN} ]] ; then
		# unfortunately cannot use the simple ${var^^} on OS-X to convert to upper case
	    local out c i
		for (( i=0;i<${#s};i++ )) ; do
			c="${s:${i}:1}"
			if [[ "${LOWER_CHARS//${c}}" != "${LOWER_CHARS}" ]] ; then
				c="${LOWER_CHARS%${c}*}"
				c="${UPPER_CHARS:${#c}:1}"
			fi
			out="${out}${c}"
		done
		echo "${out}"
	else
		echo "${s^^}"
	fi

}

function lowerCase () {

    local s="${*}"
	if [[ ${DARWIN} ]] ; then
		# unfortunately cannot use the simple ${var,,} on OS-X to convert to lower case
		local out c i t
		for (( i=0;i<${#s};i++ )) ; do
			c="${s:${i}:1}"
			if [[ "${c//[A-Z]}" != "${c}" ]] ; then
				t="${UPPER_CHARS%${c}*}"
				t="${LOWER_CHARS:${#t}:1}"
				[[ "${t}" != "" ]] && c="${t}"
			fi
			out="${out}${c}"
		done
		echo "${out}"
	else
		echo "${s,,}"
	fi

}

function PKGU_SearchLFN () {
    local s
    local f
    local o="${1}"

    local n
    local e
    [[ "${o}" != "" ]] && o="${o}/"
    # echo $o
    for s in "${o}"* ; do
        f="${s##*/}"
        n="${f%%.*}"
        # $f & $e equal then No Extension - OK
        [[ "${n}" == "${f}" ]] && e="" || e="${f#*.}"

        # Only Extension, Name > 8 or Ext > 3 then is LFN
        if [[ ${#n} -eq 0 ]] || [[ ${#n} -gt 8 ]] || [[ ${#e} -gt 3 ]] ; then
            echo "${s}"
        elif [[ -d "${s}" ]] ; then
            PKGU_SearchLFN "${s}"
        fi
    done
}

function PKGU_SearchEFN () {
    local s
    local f
    local o="${1}"

    local n
    local e
    local u
    [[ "${o}" != "" ]] && o="${o}/"
    # echo $o
    for s in "${o}"* ; do
        f="${s##*/}"
        u=$(upperCase "${f}")
        n="${f%%.*}"
        # $f & $e equal then No Extension - OK
        [[ "${n}" == "${f}" ]] && e="" || e="${f#*.}"

        # Only Extension, Name > 8 or Ext > 3 or (not all uppercase) then is EFN
        if [[ ${#n} -eq 0 ]] || [[ ${#n} -gt 8 ]] || [[ ${#e} -gt 3 ]] || [[ "${f}" != "${u}" ]]; then
            echo "${s}"
        elif [[ -d "${s}" ]] ; then
            PKGU_SearchEFN "${s}"
        fi
    done
}

function PKGU_CheckSources () {
    echo "Check Srcs: $1"
    local hwd="${PWD}"
    cd "${1}" || return 1
    local efns=$(PKGU_SearchEFN "" | wc -l)
    [[ $efns -gt 0 ]] && {
        echo "$(echo ${PWD##*/} | tr [:lower:] [:upper:]) package sources may contain LFNs or case-specific filenames"
        zip -q -r -9 -m ../SOURCES.ZIP * || return 1
        mv ../SOURCES.ZIP .
    }
    cd "${hwd}"
}

function PKGU_CheckPath () {
    echo "Check Path: $1"
    local hwd="${PWD}"
    local flag=
    [[ ! -d "${1}" ]] && return 0
    cd "${1}" || return 1
    PKGU_SearchLFN "" | while IFS=""; read line ; do
        [[ "$flag" == "" ]] && {
            flag=yes
            echo "$(echo ${PWD##*/} | tr [:lower:] [:upper:]) package '${1##*/}' contain LFNs... Compressing"
        }
        # echo ${1}/${line}
        zip -r -9 -m LFNFILES.ZIP "${line}"
    done
    cd "${hwd}"
}

function PKGU_SmashLFN () {
    local li
    local si
    for i in ${1}/* ; do
        [[ "${i}" == "${1}"'/*' ]] && continue;
        [[ ! -d "${i}" ]] && continue
        li=$(echo "${i##*/}" | tr "[:upper:]" "[:lower:]")
        if [[ "${li}" == "appinfo" ]] ; then
            continue
        elif [[ "${li}" == "bin" ]] ; then
            PKGU_CheckPath "${i}" || return 1
        elif [[ "${li}" == "links" ]] ; then
            PKGU_CheckPath "${i}" || return 1
        elif [[ "${li}" == "source" ]] ; then
            si=$(ls -1 "${i}" | grep -ia "^${1}")
            PKGU_CheckSources "${i}/${si}" || return 1
        else
            si=$(ls -1 "${i}" | grep -ia "^${1}")
            PKGU_CheckPath "${i}/${si}" || return 1
        fi;
    done
}

function PKGU_getfiles () {
    local f
    for f in * ; do
        [[ -d "$f" ]] && echo $f
    done
}

function PKGU_main () {
    MDIR="${PWD}"
    while [[ $# -gt 0 ]] ; do
        [[ "$1" == "-x" ]] && {
            shift
            LFN_ZIP=true
            continue
        }
        [[ "$1" == "-k" ]] && {
            shift
            LFN_ZIP=false
            continue
        }
        PKG="$1"
        [[ "${PKG:$(( ${#PKG} - 1 ))}" == "/" ]] && PKG="${PKG:0:$(( ${#PKG} - 1 ))}"
        PKG="${PKG##*/}"
        PKG="${PKG%.*}"
        if [[ -f "${1}" ]] ; then
            echo Expand package $PKG
            [[ "${1}" != "${1%/*}" ]] && cd "${1%/*}"
            [[ ! -d "${PKG}" ]] && {
                mkdir "${PKG}" || exit 1
                cd "${PKG}" || exit 1
                unzip -qq "../${1##*/}" || exit 1
                cd .. || exit 1
                rm "${1##*/}" || exit 1
            }
        elif [[ -d "${1}" ]] ; then
            [[ "${LFN_ZIP}" == "true" ]] && {
                PKGU_SmashLFN "${1}" || exit 1
            }
            echo Collapse package $PKG
            # ZIP=ZIP
            ZIP=zip
            LPKG=$(echo "${PKG}" | tr "[:upper:]" "[:lower:]")
            # [[ "${PKG}" == "${LPKG}" ]] && ZIP=zip
            cd "${1}" || exit 1
            zip -q -9 -r -k "../${LPKG}.${ZIP}" $(PKGU_getfiles) || {
                zip -9 -r -k "../${LPKG}.${ZIP}" $(PKGU_getfiles)
                [[ "${LFN_ZIP}" == "false" ]] && \
                    echo && echo Perhaps enable LFN Pre-compression with -x option
                exit 1
            }
            cd .. || exit 1
            rm -rf "${1}" || exit 1
        else
            echo Error: what about ${1}\?
            exit 1
        fi;
        shift;
        cd "${MDIR}"
    done;
}

PKGU_main "${@}"
