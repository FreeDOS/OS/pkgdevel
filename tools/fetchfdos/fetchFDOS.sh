#!/bin/bash

function clone () {
    # using git clone
    git clone --recursive https://github.com/FDOS/${1}.git || return 1
    return 0
}

function download () {

		# using zips
    rm ${1}.zip >/dev/null 2>&1
    curl -L https://codeload.github.com/FDOS/${1}/zip/refs/heads/master -o ${1}.zip || return 1
    unzip -o ${1}.zip || return 1
    rm ${1}.zip || return 1
    mv ${1}-master ${1} || return 1
    return 0
}

function fetch () {

	local project

	while [[ ${#*} -gt 0 ]] ; do
		project="${1}"
		shift
		[[ -d "${project}" ]] && continue
		echo
        download "${project}" || return $?
        cd "${project}" || return $?
        # update global database, files and directory timestamps
        fdvcs.sh -sda -pa
        cd .. || return $?
	done
	return 0

}

function fetchall () {

	fetch kernel country sys freecom || return 1
	fetch share xcopy fc choice more sort find label edit fdisk mem || return 1
	fetch kitten emm386 diskcopy tree || return 1

	# maintained elsewhere
	# fetch help shsucd edlin himemx || return 1

    # not updated in a while
    # fetch cdrom unformat ramdisk nlsfunc move mouse mode mirror || return 1
    # fetch himem graphics fdshield fdapm display diskcomp defrag || return 1
    # fetch cpi chkdsk devload deltree format comp cache backup || return 1
    # fetch keyb append debug ansi attrib assign || return 1

}

fetchall
if [[ $? -ne 0 ]] ; then
	echo an error occoured
	exit 1
fi

