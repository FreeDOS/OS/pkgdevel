#/bin/bash

for i in * ; do
  [[ ! -d "${i}" ]] && continue
  pushd "${i}"
  fdvcs.sh -nls -cpr -sda -pa -c 'NLS: Package and Description Update'
  popd
done

