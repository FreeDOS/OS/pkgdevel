#!/bin/bash

pushd "${1}" || exit $?
fdvcs.sh -nls
fdvcs.sh -s -c "metadata update"
mkdir -v APPINFO-NEW || exit $?
cp -v APPINFO/* APPINFO-NEW/ || exit $?
for i in APPINFO/* ; do
	git rm -f ${i} || rm -f ${i}
done
if [[ ! -d APPINFO ]] ; then
	mkdir APPINFO || exit $?
fi
for i in APPINFO-NEW/* ; do
	n="$(echo ${2}.${i##*.} | tr '[:lower:]' '[:upper:]')"
	mv -v ${i} APPINFO/${n} || exit $?
	git add APPINFO/${n} || exit $?
done
rmdir APPINFO-NEW || exit $?
fdvcs.sh -s -c "${1} package renamed to ${2}"
popd || exit $?
mv -v ${1} ${2} || exit 1
