#!/bin/bash

# download multiple software download repository CSV files and compare update
# package versions with local copy of git repository projects

# Created 2023 by Jerome Shidel v2.0
# Released to Public Domain

# Script settings
TOOL_VER=1.0

function update_mirror () {
	pushd "${i}" || return $?
	local src="$(< .gitlab-mirror-url)"
	if [[ "${src}" != '' ]] ; then
		echo "update mirror for ${1} from ${src}"
		git clone "${src}" ".gitlab-mirror-files"
	else
		echo "update mirror ${1}"
	fi
	if [[ $? -eq 0 ]] ; then
		if [[ -x ".gitlab-mirror.sh" ]] ; then
			.gitlab-mirror.sh || return $?
		else
			rm -rf *
			mv .gitlab-mirror-files/* .
			mv .gitlab-mirror-files/.timestamps .
			mv .gitlab-mirror-files/.gitignore .
		fi
		ls -ald .gitlab-mirror-files/.*
		rm -rf .gitlab-mirror-files || return $?
		git add *
		fdvcs.sh -cpr -s -pa
		echo "mirror update $(date)"
		git commit -m "mirror update $(date)" || return $?
		git push || return $?
		git ls-files -o
	fi
	popd
}

function main () {

	local i
	for i in * ; do
		if [[ -f "${i}/.gitlab-mirror-url" ]] ; then
			update_mirror "${i}" || return $?
		fi
	done

}

main "${@}"
exit $?
